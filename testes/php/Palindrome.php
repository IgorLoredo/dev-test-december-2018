<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    public static function isPalindrome($string)
    { 
      $string = preg_replace( '/[^\sa-zA-Z0-9]/', '', $string);
      if ($string == strrev($string)) {
          return 'eh';
      }
      return 'nao eh';
    }
}

echo Palindrome::isPalindrome('asa');;