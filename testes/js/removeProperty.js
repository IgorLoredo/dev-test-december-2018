// Implemente a função removeProperty, que recebe um objeto e o nome de uma propriedade.

// Faça o seguinte:

// Se o objeto obj tiver uma propriedade prop, a função removerá a propriedade do objeto e retornará true;
// em todos os outros casos, retorna falso.

function removeProperty(obj, prop) {
  if( obj.hasOwnProperty(prop)) {
    var remove = prop;
   
    /*delete obj; */
    delete obj;
    console.log(obj);
    return true;
  }
  
  return false;
}
novo = new Object();
novo = {
  nome:'novo',
  idade:'21'
}

console.log(removeProperty(novo, 'nome'));

console.log(novo);