// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
   var parts = userDate.split('/');
    if (parts[0].length == 1) parts[0] = '0' + parts[0];
    if (parts[1].length == 1) parts[1] = '0' + parts[1];
    return parts[2] + parts[0] + parts[1];
}

console.log(formatDate("12/31/2014"));